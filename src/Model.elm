module Model exposing (Model, Msg(..), init, keyDecoder, subscriptions, agentForPlayer, updateModelPlayerStats, updatePlayerStats, initPlayerStats, theGame)

import Browser.Events
import Game exposing (Game, initGame, theWinner)
import Json.Decode
import Random exposing (Seed, initialSeed)
import Referee exposing (RefereeStats, initRefereeStats, updateRefereeStats)
import Time
import Types exposing (Agent(..), Board, GoString, Move(..), Player(..), Point, RefereeReport(..))
import Utils


type alias Model =
    { game : Game
    , selfPlay : Bool
    , hotStone : Maybe GoString
    , size : Int
    , agents : (Agent, Agent)
    , playerStats : PlayerStats
    , refereeStats : RefereeStats
    , winner : Maybe Player
    , seed : Seed
    }

type Msg
    = NewGame
    | NewSize String
    | Move Move
    | HotStone Bool Point
    | Cycle Time.Posix
    | KeyDown String


type alias PlayerStats =
    { played : Int
    , blackWins : Int
    , whiteWins : Int }

initPlayerStats : PlayerStats
initPlayerStats =
    PlayerStats 0 0 0

init : () -> ( Model, Cmd Msg )
init () =
    ( { game = initGame 9
      , selfPlay = True
      , hotStone = Nothing
      , size = 9
      , agents = (EyeFool, RandomFool)
      --, agents = (RandomFool, RandomFool)
      , playerStats = initPlayerStats
      , refereeStats = initRefereeStats
      , winner = Nothing
      , seed = initialSeed 1 }
    , Cmd.none
    )


theGame : Model -> Game
theGame model =
    model.game

{-  now work out the winner and tally for the session, played incremented too
    hmm, good to ALL model updates this way, could pipeline them all then
-}
updateModelPlayerStats : Model -> Model
updateModelPlayerStats model =
    let
        winner =
            theWinner model.game
    in
        { model | playerStats = updatePlayerStats model.playerStats winner
        , winner = Just winner}

{-  gather winner stat at end of each game in a session
-}
updatePlayerStats : PlayerStats -> Player -> PlayerStats
updatePlayerStats stats winner =
    { stats
        | played = stats.played + 1
        , blackWins = stats.blackWins + (if winner == Black then 1 else 0)
        , whiteWins = stats.whiteWins + (if winner == White then 1 else 0)
        }


agentForPlayer : Model -> Agent
agentForPlayer model =
    let
        (white, black) = model.agents
    in
        case (Game.thePlayer model.game) of
            White -> white
            Black -> black


subscriptions : Model -> Sub Msg
subscriptions model =
    []
        |> Utils.addIfSet
            (Browser.Events.onAnimationFrame Cycle)
            model.selfPlay
        |> Utils.addIfSet
            (Browser.Events.onKeyDown keyDecoder)
            (not model.selfPlay)
        |> Sub.batch


keyDecoder : Json.Decode.Decoder Msg
keyDecoder =
    Json.Decode.field "key" Json.Decode.string
        |> Json.Decode.map KeyDown








