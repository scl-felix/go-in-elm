module View.Board exposing (..)

{-
    ========== View ==========

    Xlates row, col to view coords by scaling * 10
-}

import Dict
import GoString exposing (gstrColour)
import Html exposing (Html)
import Model exposing (Msg(..))
import Set
import Svg
import Svg.Attributes
import Svg.Events
import Types exposing (Board, GoString, Move(..), Player(..), Point)


{-  @todo suss our viewport scaling to width and height!
-}
boardView : Board -> Maybe GoString -> Html Msg
boardView board hotgstr =
    Html.div
        []
        [ Svg.svg
            [ Svg.Attributes.version "1.1"
            , Svg.Attributes.width "200"
            , Svg.Attributes.height "200"
            , Svg.Attributes.viewBox "0 0 200 200"
            ]
            ([ -- border
               Svg.rect
                [ Svg.Attributes.fill "grey" -- "rgba(200, 200, 200, 100)"

                --, border "2"
                , Svg.Attributes.stroke "brown"
                , Svg.Attributes.strokeWidth "3"
                , Svg.Attributes.width "1000"
                , Svg.Attributes.height "1000"
                ]
                []
             ]
                ++ boardLines board
                ++ clickRadii board
                ++ stones board
                ++ (
                    case hotgstr of
                        Nothing -> []
                        Just gstr -> hints board gstr
                )

            )
        ]

boardLines : Board -> List (Svg.Svg Msg)
boardLines board =
    let
        hlines =
            List.range 1 board.rows
                |> List.map (hline board.cols)
        vlines =
            List.range 1 board.cols
                |> List.map (vline board.rows)
    in
        hlines ++ vlines


vline : Int -> Int -> Svg.Svg Msg
vline rows x =
    Svg.line
        [ Svg.Attributes.stroke "black" -- "rgba(0, 0, 0, 100)"
        , Svg.Attributes.strokeWidth "1"
        , Svg.Attributes.x1 (String.fromInt (x * 10))
        , Svg.Attributes.y1 "10"
        , Svg.Attributes.x2 (String.fromInt (x * 10))
        , Svg.Attributes.y2 (String.fromInt (rows * 10))
        ]
        []


hline : Int -> Int -> Svg.Svg Msg
hline cols y =
    Svg.line
        [ Svg.Attributes.stroke "black" -- "rgba(0, 0, 0, 100)"
        , Svg.Attributes.x1 "10"
        , Svg.Attributes.y1 (String.fromInt (y * 10))
        , Svg.Attributes.x2 (String.fromInt (cols * 10))
        , Svg.Attributes.y2 (String.fromInt (y * 10))
        ]
        []

-- use 1:9 as row,col for Points being placed on (onClick), 10:90 for display
clickRadii : Board -> List (Svg.Svg Msg)
clickRadii board =
    List.range 1 board.rows
    |> List.concatMap
        (\r ->
            List.range 1 board.cols
            |> List.map (\c -> (r, c)))
    |> List.map
        (\(row, col) ->
            Svg.circle
            [ Svg.Attributes.stroke "turquoise"
            , Svg.Attributes.fill "grey"
            , Svg.Attributes.strokeWidth "1"
            , Svg.Events.onClick (Move (Play (row, col)))
            , Svg.Attributes.cx (String.fromInt (row * 10))
            , Svg.Attributes.cy (String.fromInt (col * 10))
            , Svg.Attributes.r (String.fromInt 4)]
            [])


stones : Board -> List (Svg.Svg Msg)
stones board =
    board.grid
    |> Dict.toList
    |> List.map svgStone

svgStone : ((Int, Int), GoString) -> Svg.Svg Msg
svgStone ((row, col), gstr) =
    --let
    --    _ = Debug.log "drawing stone at " (row, col)
    --in
    Svg.circle
        [ Svg.Attributes.stroke (gstrColour gstr)
        , Svg.Attributes.fill (gstrColour gstr)
        , Svg.Attributes.strokeWidth "1"
        , Svg.Events.onMouseOver (HotStone True (row, col))
        , Svg.Events.onMouseOut (HotStone False (row, col))
        , Svg.Attributes.cx (String.fromInt (row * 10))
        , Svg.Attributes.cy (String.fromInt (col * 10))
        , Svg.Attributes.r (String.fromInt 4)]
        []

{- for gstr show
        - stones with small yellow circles
        - liberties with small green circles
-}
hints : Board -> GoString -> List (Svg.Svg Msg)
hints board gstr = (
        gstr.stones
        |> Set.toList
        |> List.map svgHotStone )
    ++ ( gstr.liberties
        |> Set.toList
        |> List.map svgLiberty )

-- put a small yellow dot in the middle
-- also keep event handlers, else get amouseOut
svgHotStone: Point -> Svg.Svg Msg
svgHotStone (row, col) =
    Svg.circle
        [ Svg.Attributes.stroke "yellow"
        , Svg.Attributes.fill "yellow"
        , Svg.Attributes.strokeWidth "1"
        , Svg.Events.onMouseOver (HotStone True (row, col))
        , Svg.Events.onMouseOut (HotStone False (row, col))
        , Svg.Attributes.cx (String.fromInt (row * 10))
        , Svg.Attributes.cy (String.fromInt (col * 10))
        , Svg.Attributes.r (String.fromInt 2)]
        []

-- put a small yellow dot in the middle
svgLiberty: Point -> Svg.Svg Msg
svgLiberty (row, col) =
    Svg.circle
        [ Svg.Attributes.stroke "green"
        , Svg.Attributes.fill "green"
        , Svg.Attributes.strokeWidth "1"
        , Svg.Attributes.cx (String.fromInt (row * 10))
        , Svg.Attributes.cy (String.fromInt (col * 10))
        , Svg.Attributes.r (String.fromInt 2)]
        []
