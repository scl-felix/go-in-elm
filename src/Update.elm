module Update exposing (..)

import Agent
import Board exposing (getGoString)
import Game exposing (initGame, isGameOver, makeMove, theWinner)
import Model exposing (Model, Msg(..), agentForPlayer, initPlayerStats, theGame, updateModelPlayerStats, updatePlayerStats)
import Referee exposing (updateRefereeStats)
import Types exposing (Agent(..), RefereeReport(..))


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of

        -- now can resize
        NewGame ->
            ( { model
                | game = Game.initGame model.size
                , refereeStats = updateRefereeStats (NotMoved "White always to start...") model.refereeStats}
            , Cmd.none )

         --dont actually resize yet
        NewSize usr ->
            let
                gnu = (String.toInt usr)
                    |> Maybe.withDefault 9
            in
                ( { model
                    | size = gnu
                    , refereeStats = updateRefereeStats (NotMoved "Ok - Hit Go! for new board.") model.refereeStats }
                , Cmd.none )

        -- from a human player, @todo CHECK are we using Human agent? selfPlay?
        Move m ->
            let
                (gnu, ref) = makeMove model.game m
            in
            ( { model
                | game = gnu
                , refereeStats = updateRefereeStats ref model.refereeStats }
            , Cmd.none )

        HotStone True pt ->
            ( { model
                | hotStone
                    = getGoString model.game.current.board pt }
            , Cmd.none )

        HotStone False _ ->
            ( { model | hotStone = Nothing }, Cmd.none )

        -- self play will use an Agent for each colour
        Cycle posix ->
            if model.selfPlay then
                if not (isGameOver model.game) then
                    let
                        -- Agent's move and all ref reports for the trialed moves up until
                        -- Agent Maybe found a valid play
                        ((move, nextSeed), trialMoveReports) =
                            Agent.selectMove (agentForPlayer model) model.game model.seed

                        -- update model's ref stats with reports on invalid trial moves (so we
                        -- can see self-captures and Ko violations)
                        trialStats =
                            trialMoveReports
                            |> List.foldl updateRefereeStats model.refereeStats

                        trialModel =
                            { model | refereeStats = trialStats}

                        -- make move for Agent and update referee stats with report on move
                        (gnuGame, moveReport) =
                            Game.makeMove (theGame trialModel) move
                    in
                        ( { trialModel
                            | game = gnuGame
                            , refereeStats = updateRefereeStats moveReport trialModel.refereeStats
                            , seed = nextSeed }
                        , Cmd.none)

                -- game over - lets have 1000 per session to start with
                else
                    let
                        gnuModel =
                            updateModelPlayerStats model
                    in
                    if gnuModel.playerStats.played > 1000 then
                        -- session over, go back to non self play
                        ( { gnuModel
                                | selfPlay = False
                                , game = initGame gnuModel.size
                                , agents = (Human, Human)
                                , playerStats = initPlayerStats
                          }
                        , Cmd.none )
                    else
                        -- play again!
                        ( { gnuModel
                            | game = initGame model.size
                            --, playerStats = updatePlayerStats model.playerStats (theWinner model.game)
                            }
                        , Cmd.none )

            -- Human vs Human, moves handled by the Move msg
            else
                ( model, Cmd.none )

        KeyDown string ->
            ( model, Cmd.none )
