module Agent exposing (..)

import Board
import Game exposing (Game, theBoard, thePlayer)
import Random exposing (Seed)
import Random.List
import Types exposing (Agent(..), Move(..), RefereeReport(..), allPoints)

selectMove : Agent -> Game -> Seed -> ((Move, Seed), List RefereeReport)
selectMove agent game seed =
    case agent of
        Human ->
            ((None, seed), [NotMoved "Waiting..."])
        RandomFool ->
            (selectFoolMove game seed)
        EyeFool ->
            selectEyeFoolMove game seed

{-  for all squares on board
    - map all the free pts
    - filter just valid moves
    - pick one
-}
selectFoolMove : Game -> Seed -> ((Move, Seed), List RefereeReport)
selectFoolMove game seed =
    let
        board = theBoard game
        pts = allPoints board.rows board.cols
        rndGen = Random.List.shuffle pts
        (rndPts, nextSeed)
            = Random.step rndGen seed
        --emptyPts
        (possValidPlay, refReports)
            = rndPts
                |> List.filter (Board.isFree board)
        -- (Bool, List RefereeReport)
        --    = emptyPts
                |> List.map (\pt -> (Play pt))
                |> List.foldl (keepReportUntilValidMove game) (Nothing, [])
    in
        --let
        --    _ = Debug.log "REF REPORTS AFTER selecting fool move" refReports
        --in
        case possValidPlay of
            Nothing ->
                ((Pass, nextSeed), refReports)
            Just mv ->
                ((mv, nextSeed), refReports)

keepReportUntilValidMove : Game -> Move -> (Maybe Move, List RefereeReport) -> (Maybe Move, List RefereeReport)
keepReportUntilValidMove game move (validPlay, report) =
    case validPlay of
        -- we are done, already found a valid move so ignore this one ... (lazy eval ?)
        Just _ ->
            (validPlay, report)

        -- not done yet, so is this the first valid move
        Nothing ->
            let
                (isValid, refSays) = Game.isValidMove game move
            in
            if isValid then
                -- got the first valid move, will be recorded when actually made so dont count this trial
                (Just move, report)
            else
                -- nope, carry on but keep the refs report for testing rule violations are detected!
                (Nothing, refSays::report)


selectEyeFoolMove : Game -> Seed -> ((Move, Seed), List RefereeReport)
selectEyeFoolMove game seed =
    selectFoolMove game seed
