module View exposing (..)

-- generate a Place event from mouse click on a specific 'cell' or junction
-- * a point being the centre of a circle sensitive to clicks on each junction


import Game exposing (theBoard, thePlayer, theWinner)
import Html.Attributes
import Html.Events
import Types exposing (Move(..), labelFor)
import View.Board exposing (boardView)
import Html exposing (Html)
import Model exposing (Model, Msg(..))

view : Model -> Html Msg
view model =
    Html.div
        []
        [ Html.text ("Go! self play, 1000 rounds testing random idiot agents against each other")
        , Html.br [] []
        , Html.text ("TODO : choose agents, allow Human v Human and Human v Agent")
        , gameButtons model
        , boardView (theBoard model.game) model.hotStone
        , gameStats model
        , moveStats model
        ]

gameButtons : Model -> Html Msg
gameButtons model =
    Html.div
    []
    [ Html.button
        [Html.Events.onClick NewGame]
        [Html.text "Go!"]
    , Html.button
        [Html.Events.onClick (Move Pass)]
        [Html.text "Pass"]
    , Html.button
        [Html.Events.onClick (Move Resign)]
        [Html.text "Resign"]
    --, Html.input
    --    [ Html.Events.onInput NewSize
    --    , Html.Attributes.value (String.fromInt model.size)]
    --    [ Html.text "Board Size"]
    ]

moveStats : Model -> Html Msg
moveStats model =
    let
        refs = model.refereeStats
    in
    Html.div
    []
    [ Html.text("Violations - total occurrences ")
    , Html.br [] []
    , Html.text ("Ko............." ++ String.fromInt (refs.koBlack + refs.koWhite))
    , Html.br [] []
    , Html.text ("Self capture..." ++ String.fromInt (refs.selfCapBlack + refs.selfCapWhite))
    , Html.br [] []
    , Html.text ("Passes........." ++ String.fromInt (refs.passBlack + refs.passWhite))
    , Html.br [] []
    ,  Html.text ("Resign........." ++ String.fromInt (refs.resignBlack + refs.resignWhite))
    , Html.br [] []
    ]

gameStats : Model -> Html Msg
gameStats model =
    let
        game =
            model.game
        player =
            thePlayer game
        winner =
            model.winner
            |> Maybe.map labelFor
            |> Maybe.withDefault ""
        bwins = model.playerStats.blackWins |> String.fromInt
        wwins = model.playerStats.whiteWins |> String.fromInt
        (w, b) = model.game.current.board.captures
    in
    Html.div
    []
    [ Html.text ("Turn: " ++ (player |> labelFor))
    , Html.br [] []
    , Html.text ("Last winner " ++ winner)
    , Html.br [] []
    , Html.text ("Captures: Black " ++ (String.fromInt w) ++ ", White " ++ (String.fromInt b))
    , Html.br [] []
    , Html.text ("Total White wins " ++ wwins)
    , Html.br [] []
    , Html.text ("Total Black wins " ++ bwins)
    ]
