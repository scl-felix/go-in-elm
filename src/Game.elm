module Game exposing (..)

import Board exposing (territory)
import List.Extra
import Set
import Types exposing (Board, GoString, Move(..), Player(..), Point, RefereeReport(..), labelFor, other)

{-  Hmmm - could do this with a single stack - tos is the current state
    but current is a handy place to keep state whilst evaluating self-capture
    and Ko
-}
type alias Game =
    { current : State
    , history : (List State) }

type alias State =
    { board : Board
    , player : Player
    , move : Move }

initGame : Int -> Game
initGame size =
    let
        is = initState size
    in
    Game is [is]

initState : Int -> State
initState size =
    State
        (Board.empty size size)
        White
        None

theBoard : Game -> Board
theBoard game =
    game.current.board

thePlayer : Game -> Player
thePlayer game =
    game.current.player

theMove : Game -> Move
theMove game =
    game.current.move

isGameOver : Game -> Bool
isGameOver gs =
    case theMove gs of
        None ->
            False
        Resign ->
            True
        Pass ->
            (prevMove gs == Pass)
        Play _ ->
            False

theWinner : Game -> Player
theWinner game =
    let
        (w, b) = territory game.current.board
    in
        case w > b of
            True -> White
            False -> Black

{-  game state tells us the board position, and which
    player the move it is for -}
makeMove : Game -> Move -> (Game, RefereeReport)
makeMove game move =
    let
        (ok, report) = isValidMove game move
        --_ = Debug.log "Making move" move
    in
    if ok
        then
            let
                gnu = applyMove game.current move
            in
            ( { game
                    | current = gnu
                    , history = gnu :: game.history }
            , report )
        else
            ( game, report)


isValidMove : Game -> Move -> (Bool, RefereeReport)
isValidMove gs move =
    let
        player =
            thePlayer gs
    in
    if (isGameOver gs) then
        (False, GameOver ("Game over - win for " ++ (labelFor <| thePlayer gs)))
    else
        case
            move
        of

        -- check a stone placement
        Play pt ->
            case Board.getGoString (theBoard gs) pt of

                -- pt is free, try the move and see if it was legal
                Nothing ->
                    if isSelfCapture gs move then
                        (False, SelfCapture player "Nah - self-capture is just stupid. Also, not allowed!")
                        --|> Debug.log "SELF CAPTURE"

                    else if (isKoViolation gs move) then
                        (False, KoViolation player "Ooh - eternity beckons - that's a Ko violation, not allowed!")
                        --|> Debug.log "KO VIOLATED"
                    else
                        (True, Valid move player "")

                -- oops - pt should be unoccupied
                Just _ ->
                    (False, Cheating player "Ouch - blatant cheating! Not allowed!")
                    |> Debug.log "CHEATING!!!"

        -- can never happen, a placeholder initial value
        None ->
            (False, NotMoved "That's fucked up - call Fi")
            |> Debug.log "WTF !!!"

        -- pass and resign always valid
        _ ->
            (True, Valid move player "")

applyMove : State -> Move -> State
applyMove state move =
    case move of
        Play pt ->
            { state
                | board = Board.placeStone state.board state.player pt
                , player = other state.player
                , move = move }
        None ->
            state
        _ ->
            { state
                | player = other state.player
                , move = move }

isSelfCapture : Game -> Move -> Bool
isSelfCapture gs move =
    case move of
        Play pt ->
            let
                player =
                    thePlayer gs
                board =
                    theBoard gs
                gnu =
                    Board.placeStone board player pt
                selfCapture =
                    Board.getGoString gnu pt
                        |> Maybe.andThen (\p -> Just (Set.size p.liberties == 0))
                        |> Maybe.withDefault False
                --_ = if selfCapture then
                --        Debug.log "SELF CAPTURE "
                --    else
                --        (\_ -> False)

            in
                selfCapture
        _ ->
            False

-- for Ko comparisons only need grid and player from game state
isKoViolation : Game -> Move -> Bool
isKoViolation gs move =
    case move of
        Play pt ->
            let
                gnuState = applyMove gs.current move
            in
                checkForSameState gnuState gs.history
        _ ->
            False

checkForSameState : State -> List State -> Bool
checkForSameState state states =
    let
        isOne =
            states
            |> List.Extra.find (\s ->
                    s.player == state.player && s.board.grid == state.board.grid
                )
            |> Maybe.andThen (\_ -> Just True)
            |> Maybe.withDefault False

        --_ = if isOne then
        --        Debug.log "SELF KO VIOLATION "
        --    else
        --        (\_ -> False)
    in
        isOne


prevMove : Game -> Move
prevMove { current, history } =
    case history of
        prev :: _ ->
            prev.move
        [] ->
            None
