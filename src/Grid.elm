module Grid exposing (..)

import Dict exposing (Dict)
import Set exposing (Set)
import Types exposing (GoString, Grid, Player(..), Point, allPoints, isSame)


empty : Grid
empty =
    Dict.empty

getGoString : Grid -> Point -> Maybe GoString
getGoString gd pt =
   Dict.get pt gd

putGoString : Grid -> GoString -> Grid
putGoString grid gstr =
    Dict.insert gstr.point gstr grid



-- propagates libs and stones to all other stones in the gstr
-- and persists gstr and it's updated stones to the grid
-- NB gstr is itself in it's stones. so no need to add it separately
-- BUT has no gstr so we have to add it! doh
updateGoStringOnGrid : GoString -> Grid -> Grid
updateGoStringOnGrid gstr grid =
    gstr.stones
    |> Set.toList
    |> List.foldl (updateMember gstr) grid

    --Dict.insert pt gstr dict

-- for member at pt update it's libs and stones to same as gstr
updateMember : GoString -> Point -> Grid -> Grid
updateMember gstr pt grid =
        case getGoString grid pt of
            Nothing -> grid
            Just member ->
                Dict.insert
                    pt
                    { member
                        | point = pt
                        , liberties = gstr.liberties
                        , stones = gstr.stones }
                    grid

-- hmm - removeGoStringFromGrid... how would that works?
-- no homogenising so giving liberties all done bt Board
removeFromGrid : Point -> Grid -> Grid
removeFromGrid pt dict =
    Dict.remove pt dict

samePlayer : Grid -> Player -> Point -> Bool
samePlayer grid player point =
    case
        Dict.get point grid
    of
        Nothing -> False
        Just gstr -> isSame gstr.colour player

territory : Grid -> (Int, Int)
territory grid =
    let
        gstrs =
            grid
            |> Dict.keys
            |> List.filterMap (getGoString grid)
    in
    ( gstrs |> List.filter (\gstr -> gstr.colour == White) |> List.length
    , gstrs |> List.filter (\gstr -> gstr.colour == Black) |> List.length )
