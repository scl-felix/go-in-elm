module Board exposing (..)

import GoString exposing (addLib, join, takeLib)
import Grid
import Set exposing (Set)
import Types exposing (Board, GoString, Player(..), Point, neighbours, other)
import Utils exposing (addJustValue)

empty : Int -> Int -> Board
empty rows cols =
    Board rows cols Grid.empty (0, 0)

getGoString : Board -> Point -> Maybe GoString
getGoString bd pt =
   Grid.getGoString bd.grid pt

placeStone : Board -> Player -> Point -> Board
placeStone brd pl pt =
    let
        onGrid = neighbours pt
            |> List.filter (isOnGrid brd)
            --|> Debug.log "neighbouring points on grid"
        libs = onGrid
            |> List.filter (isFree brd)
            --|> Debug.log "neighbouring free points"
        sames = onGrid
            |> List.filter (isSamePlayer brd pl)
            --|> Debug.log "each neighbouring same player stone's pt"
        others = onGrid
            |> List.filter (isSamePlayer brd (other pl))
            --|> Debug.log "each neighbouring other player stones' point"

        -- new string for this new stone
        gnustr =
            GoString pt (Set.fromList [pt]) (Set.fromList libs) pl
            --|> Debug.log "NEW GoString "

        -- merge all same neighbours into gnustr
        gnuJoined = sames
            |> List.map (getGoString brd)
            |> List.foldl addJustValue []
            |> List.foldl join gnustr
            --|> Debug.log "Joined with neighbouring same stones "

        -- place new gstr onto board AND update all it's members (gstrs)  with new libs and stones
        brdWithNewGStrJoined =  brd
            |> placeOnBoard gnuJoined
            |> updateStonesOnBoard gnuJoined
            --|> Debug.log "BOARD UPDATED - with placed stone joining any neighbouring groups"

        -- now remove this new stone's point from liberties of the neighbouring
        -- stones of other players
        othersLessLibs = others
            |> List.map (getGoString brdWithNewGStrJoined)
            |> List.foldl addJustValue []
            |> List.map (takeLib pt)
            --|> Debug.log "neighbouring other player stones: this point liberty removed "

        -- update every adjusted other stone, also propagating stones and libs to each's stones.
        brdLessLibOfPlacedStone = othersLessLibs
            |> List.foldl updateStonesOnBoard brdWithNewGStrJoined
            --|> Debug.log "BOARD OTHERS UPDATED - with placed stone removed from libs"

        -- any other's stones now with 0 liberties can be removed
        -- NB only seeing the 'head' of gstrs adjacent to new stone, could be many and
        --    all need to be propagated and each liberty given to it's neigbours!
        --    yup - removal is tricky :-)
        -- NEED to give each removed stone's point to it's neighbours as a liberty
        gnuBrd = othersLessLibs
            |> List.filter noLibs
            |> List.foldl removeFromBoard brdLessLibOfPlacedStone
            --|> Debug.log "BOARD UPDATED - with captures removed "
    in
        --gnuBrdLessLibs
        gnuBrd

placeOnBoard : GoString -> Board -> Board
placeOnBoard gstr board =
    --let
    --    _ = Debug.log "Placing gstr on grid at point: " gstr.point
    --in
    { board |
        grid = Grid.putGoString board.grid gstr
    }


-- Grid handles homogenising GoString stones ;-)
updateStonesOnBoard : GoString -> Board -> Board
updateStonesOnBoard gstr board =
    {board | grid = Grid.updateGoStringOnGrid gstr board.grid}

{-
    -- all gstr stones need removing from grid
    -- so every stones' neighbours get back a liberty
-}
removeFromBoard  : GoString -> Board -> Board
removeFromBoard gstr board =
    let
        -- first, a list of pts for the captured stones
        capturedPts =
            gstr.stones
                |> Set.toList
        count =
            List.length capturedPts
        -- so - pass in the captured points and have all their neighbours
        -- get the point as a liberty, also updates each neighbours gstr.stones
        gnuBoard =
            capturedPts
            |> List.map (\capPt -> (capPt, neighbours capPt))
            |> List.foldl addLiberties board
            |> addCaptures count gstr.colour
    in
        removeCapturedStonesFromBoard gstr gnuBoard

addCaptures : Int -> Player -> Board -> Board
addCaptures pts player board =
    let
        (w, b) = board.captures
    in
    { board
        | captures =
            case player of
                White ->
                    (w + pts, b)
                Black ->
                    (w, b + pts)
    }

removeCapturedStonesFromBoard : GoString -> Board -> Board
removeCapturedStonesFromBoard gstr board =
    let
        grd =
            Set.toList gstr.stones
            |> List.foldl Grid.removeFromGrid board.grid
    in
    {board | grid = grd}


{-  Ah-ha!
    So - Python version (i think) exploits shared GoStrings in a dictionary - ie shared state
    Elm has to either duplicate that (with a 2nd dictionary with the Pt -> GoString as a many -> 1
    Or
    Just propagate to all gstr's stones every time a lib is added (or indeed removed)
    AND remember - NOT to use an old list of real neighbours - get them from the updated grid
    every time by passing only their Point coord along
-}
addLibAndUpdateBoard : Point -> GoString -> Board -> Board
addLibAndUpdateBoard pt gstr board =
    updateStonesOnBoard (addLib pt gstr) board
    --|> Debug.log ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> updated head gstr and its stones on board "




-- give pt as a liberty to neighbouring pts on board
addLiberties : (Point, List Point) -> Board -> Board
addLiberties (pt, pts) board =
    pts
    |> List.map (getGoString board)
    |> List.foldl addJustValue []
    --|> List.concatMap (realNeighbours board)
    -- each real neighbour is a gstr so it a) get the pt and b) propagates it to members
    |> List.foldl (addLibAndUpdateBoard pt) board

realNeighbours : Board -> Point -> List GoString
realNeighbours board pt =
    neighbours pt
    |> List.map (getGoString board)
    |> List.foldl addJustValue []

noLibs : GoString -> Bool
noLibs gstr =
    Set.isEmpty gstr.liberties


isOnGrid : Board -> Point -> Bool
isOnGrid {rows, cols, grid} (row, col) =
    row > 0 && row <= rows && col > 0 && col <= cols

isFree : Board -> Point -> Bool
isFree board point =
    getGoString board point
    |> Maybe.andThen (\_ -> Just False)
    |> Maybe.withDefault True

isSamePlayer : Board -> Player -> Point ->  Bool
isSamePlayer board player point =
    Grid.samePlayer board.grid player point

territory : Board -> (Int, Int)
territory board =
    Grid.territory board.grid
