module Referee exposing (RefereeStats, initRefereeStats, updateRefereeStats)

import Types exposing (Move(..), Player(..), RefereeReport(..))
type alias RefereeStats =
    { koWhite : Int
    , koBlack : Int
    , selfCapWhite : Int
    , selfCapBlack : Int
    , passWhite : Int
    , passBlack : Int
    , resignWhite : Int
    , resignBlack : Int
    , playsWhite : Int
    , playsBlack : Int
    }

initRefereeStats : RefereeStats
initRefereeStats =
    RefereeStats 0 0 0 0 0 0 0 0 0 0

{-  gather ref report after each move, even trial moves by Agents
    so we know they are being detected -}
updateRefereeStats : RefereeReport -> RefereeStats -> RefereeStats
updateRefereeStats rep stats =
    case rep of
        SelfCapture player comment -> updateSelfCaptureViolations stats player
        KoViolation player comment -> updateKoViolations stats player
        Cheating player comment -> stats
        GameOver comment -> stats
        NotMoved comment -> stats
        Valid move player comment -> updateValidMove stats move player

updateKoViolations : RefereeStats -> Player -> RefereeStats
updateKoViolations stats player =
    case player of
        White ->
            { stats | koWhite = stats.koWhite + 1 }
        Black ->
            { stats | koBlack = stats.koBlack + 1 }

updateSelfCaptureViolations : RefereeStats -> Player -> RefereeStats
updateSelfCaptureViolations stats player =
    case player of
        White ->
            { stats | selfCapWhite = stats.selfCapWhite + 1 }
        Black ->
            { stats | selfCapBlack = stats.selfCapBlack + 1 }

updateValidMove : RefereeStats -> Move -> Player -> RefereeStats
updateValidMove stats move player =
    case move of
        Play pt ->
            updatePlay stats move player
        Pass ->
            updatePass stats player
        Resign ->
            updateResign stats player
        None ->
            stats


updatePlay : RefereeStats -> Move -> Player -> RefereeStats
updatePlay stats move player =
    case player of
        White ->
            { stats | playsWhite = stats.playsWhite + 1 }
        Black ->
            { stats | playsBlack = stats.playsBlack + 1 }

updatePass : RefereeStats -> Player -> RefereeStats
updatePass stats player =
    case player of
        White ->
            { stats | passWhite = stats.passWhite + 1 }
        Black ->
            { stats | passBlack = stats.passBlack + 1 }

updateResign : RefereeStats -> Player -> RefereeStats
updateResign stats player =
    case player of
        White ->
            { stats | resignWhite = stats.resignWhite + 1 }
        Black ->
            { stats | resignBlack = stats.resignBlack + 1 }

