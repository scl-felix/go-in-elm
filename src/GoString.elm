module GoString exposing (..)
{-
    GoString

    Represents a group of connected stones of the same colour,
    each stone in the group maintains a set of the stone's liberties
    and a set of all stones in the group

    Use cases are:
        * to add a liberty
        * remove a liberty
        * join two groups -union'ing stones, liberties then
          remove stones from liberties
-}
import Set
import Types exposing (GoString, Player(..), Point)

addLib : Point -> GoString -> GoString
addLib pt gs =
    { gs | liberties = Set.insert pt gs.liberties }

takeLib : Point -> GoString -> GoString
takeLib pt gs =
    { gs | liberties = Set.remove pt gs.liberties }

join : GoString -> GoString -> GoString
join gnu gs =
--join gs gnu =
    let
        allStones = Set.union gs.stones gnu.stones
        allLibs = Set.union gs.liberties gnu.liberties
        allLibsLessStones = Set.diff allLibs allStones
    in
    { gs
        | stones = allStones
        , liberties = allLibsLessStones }

gstrColour : GoString -> String
gstrColour gstr =
    case
        gstr.colour
    of
        White -> "white"
        Black -> "black"

