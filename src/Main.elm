module Main exposing (..)

import Browser
import Html
import Model exposing (init, subscriptions)
import Update exposing (update)
import View exposing (view)

main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
