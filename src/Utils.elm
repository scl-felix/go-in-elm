module Utils exposing (..)

addIfSet : a -> Bool -> List a -> List a
addIfSet a doit list =
    if doit then
        a :: list
    else
        list

addJustValue : Maybe a -> List a -> List a
addJustValue a lst =
    case a of
        Nothing -> lst
        Just val -> val :: lst

