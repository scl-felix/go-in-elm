module Types exposing (..)

import Dict exposing (Dict)
import Set exposing (Set)

type alias Grid = Dict Point GoString

type Player
    = White
    | Black

other : Player -> Player
other p =
    case p of
        Black -> White
        White -> Black

isSame : Player -> Player -> Bool
isSame p1 p2 =
    p1 == p2

labelFor : Player -> String
labelFor player =
    case player of
        Black -> "Black"
        White -> "White"

type Agent
    = Human
    | RandomFool
    | EyeFool

-- type alias not comparable, this is - so ok for Set
type alias Point = (Int, Int)

neighbours : Point -> List Point
neighbours (row, col) =
    [ (row + 1, col)
    , (row - 1 , col)
    , (row, col + 1)
    , (row, col - 1) ]

allPoints : Int -> Int -> List Point
allPoints rows cols =
    List.range 1 rows
    |> List.foldl (\row lst -> (rowCols row cols) :: lst) []
    |> List.concat

rowCols : Int -> Int -> List Point
rowCols row cols =
    List.range 1 cols
    |> List.map (\col -> (row, col))

type Move
    = None
    | Pass
    | Resign
    | Play Point

type alias GoString =
    { point : Point
    , stones : Set Point
    , liberties : Set Point
    , colour : Player }

type alias Board =
    { rows : Int
    , cols : Int
    , grid : Grid
    , captures : (Int, Int) }

type RefereeReport
    = SelfCapture Player String
    | KoViolation Player String
    | Cheating Player String
    | GameOver String
    | NotMoved String
    | Valid Move Player String

